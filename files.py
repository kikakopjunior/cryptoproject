import aiofiles
import os
import json

class Files:
    def __init__(self) -> None:
        self.current_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "fetch_api")
    
    async def init(self):
        if not os.path.exists(self.current_dir):
            os.mkdir(self.current_dir)

    async def file_exists(self, filename:str) -> bool:
        return os.path.exists(os.path.join(self.current_dir, filename))

    async def write(self, filename:str, data:dict, append=False):
        path = os.path.join(self.current_dir, filename)
        mode = 'a' if append else 'w'
        async with aiofiles.open(path, mode=mode) as file:
            for key, value in data.items():
                await file.write(f"{key}={value}\n")
            return True
    
    async def insert_list(self, filename:str, data:list):
            path = os.path.join(self.current_dir, filename)
            async with aiofiles.open(path, mode='a') as file:
                for item in data:
                    # Supposons que chaque 'item' est un dictionnaire
                    await file.write(json.dumps(item) + '\n')
                return 

    async def get_list(self, filename:str) -> list:
        if await self.file_exists(filename):
            path = os.path.join(self.current_dir, filename)
            async with aiofiles.open(path, mode='r') as file:
                lines = await file.readlines()
                return [json.loads(line.strip()) for line in lines]
        else:
            raise FileNotFoundError(f"Le fichier '{filename}' n'existe pas.")

    async def read(self, filename:str) -> dict:
        if await self.file_exists(filename):
            path = os.path.join(self.current_dir, filename)
            async with aiofiles.open(path, mode='r') as file:
                return {line.split('=')[0].strip(): line.split('=')[1].strip() for line in await file.readlines()}
        else:
            raise FileNotFoundError(f"Le fichier '{filename}' n'existe pas.")

    async def update(self, filename:str, key:str, value):
        if await self.file_exists(filename):
            data = await self.read(filename)
            data[key] = value
            await self.write(filename, data)
        else:
            raise FileNotFoundError(f"Le fichier '{filename}' n'existe pas pour mise à jour.")

    async def delete(self, filename:str):
        if await self.file_exists(filename):
            os.remove(os.path.join(self.current_dir, filename))
            return True
        else:
            raise FileNotFoundError(f"Le fichier '{filename}' n'existe pas pour suppression.")
