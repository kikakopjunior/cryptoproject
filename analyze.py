import files 
from api import Api
import asyncio

class Analyze:
    def __init__(self, *args) -> None:
        self.args = args
        self.file = files.Files()

    async def start_init(self):
        await self.file.init()  # Assurez-vous que le répertoire existe
        async with Api() as api:
            res_api = await api.get_coin_list()
            await self.file.insert_list('test.txt', res_api)
async def main():
    an = Analyze()
    await an.start_init()

asyncio.run(main())
