import aiohttp
import asyncio

class Api:
    def __init__(self) -> None:
        self.base_url = 'https://api.coingecko.com/api/v3'
        self.session = None

    async def __aenter__(self):
        self.session = aiohttp.ClientSession()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.session.close()

    async def get_ping(self):
        """route test"""
        async with self.session.get(f'{self.base_url}/ping') as resp:
            json = await resp.json()
            return json

    async def get_coin_list(self):
        """recupere la list des cryptos"""
        async with self.session.get(f'{self.base_url}/coins/list') as response:
            json = await response.json()
            return json
        
    async def get_coin_id(self, id:str):
        """recupere les donnes a partir de l'id"""
        async with self.session.get(f"{self.base_url}/coins/{id}") as response:
            json = await response.json()
            return json